import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import exam.exam1;

public class test_exam1_NoParameters {
	exam1 exam;
	@Before
	public void before() {
		exam = new exam1();
	}
	@Test
	public void test_exam_0() {
		boolean result = exam.exam(0);
		boolean expected = false;
		if(expected == false) assertFalse(result); else assertTrue(result);
	}
	@Test
	public void test_exam_1() {
		boolean result = exam.exam(1);
		boolean expected = false;
		if(expected == false) assertFalse(result); else assertTrue(result);
	}
	@Test
	public void test_exam_2() {
		boolean result = exam.exam(9);
		boolean expected = false;
		if(expected == false) assertFalse(result); else assertTrue(result);
	}
	@Test
	public void test_exam_3() {
		boolean result = exam.exam(2);
		boolean expected = true;
		if(expected == false) assertFalse(result); else assertTrue(result);
	}
}
