import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import exam.exam1;

@RunWith(value = Parameterized.class)
public class test_exam1 {
	int n;
	boolean expected;
	
	public test_exam1(int n,boolean expected) {
		this.n = n;
		this.expected = expected;
	}
	@Parameters
	public Collection<Object[]> parameters(){
		return Arrays.asList(new Object[][] {
			{0,false},
			{1,false},
			{9,false},
			{2,true},
		});
	}
	@Test
	public void test_exam_1() {
		boolean result = exam1.exam(n);
		if(expected == false) assertFalse(result); else assertTrue(result);
	}

}
